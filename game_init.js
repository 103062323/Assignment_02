var game = new Phaser.Game(400,400,Phaser.AUTO,"canvas");

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('main', mainState);

// Start the 'boot' state
game.state.start('boot');

// Define our global variable
game.global = { score: 0 ,text_: []}; 