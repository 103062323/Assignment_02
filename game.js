var mainState = 
{
    preload: function() {
        
    },

    create: function() 
    {
        game.global.score = 0;
        game.add.image(0, 0, 'background_in');

        this.backSound = game.add.audio('music',1,true);
        this.backSound.play();
        
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();//上下左右
        this.platforms = [];//開陣列給平台群
        
        this.status = 'ingame';
        this.die = false;

        //文字段
        this.text1 = null;
        this.text2 = null;
        this.text3 = null;
        this.text4 = null;

        //關於計分
        this.lastTime = 0;
        this.count_time = 0;
        this.distance = 0;
        this.count = 0;

        //音檔
        this.deadSound = game.add.audio('dead');
        this.jumpSound = game.add.audio('jump_');
        this.normalSound = game.add.audio('futsu');
        this.hurtSound = game.add.audio('hurt');
        this.verseSound = game.add.audio('verse');
        this.earSound = game.add.audio('ear');
        
        //function   
        this.Bounders();
        this.Player();
        this.Texts();

    },

    /*create_test: function()
    {
        this.platform1 = game.add.sprite(200, 200, 'normal'); 
        game.physics.arcade.enable(this.platform1);
        this.platform1.body.immovable = true;
        
    },*/
            
    Bounders: function () 
    {
        //右牆面
        this.right_bounder = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(this.right_bounder);
        this.right_bounder.body.immovable = true;

        this.Left_bounder = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.Left_bounder);
        this.Left_bounder.body.immovable = true;
    
        //天花板
        this.upper_bounder = game.add.image(0, 0, 'ceiling');
    },

    Player: function() 
    {
        this.player = game.add.sprite(game.width/2, game.height/3.2, 'player');
        this.player.direction = 10;
        game.physics.arcade.enable(this.player);
        this.player.enableBody = true; 
        this.player.body.gravity.y = 540;

        //open-source的動畫圖片
        this.player.animations.add('left', [0, 1, 2, 3], 8, true);
        this.player.animations.add('right', [9, 10, 11, 12], 8 , true);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12, true);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12, true);
        this.player.animations.add('fly', [36, 37, 38, 39], 12, true);

        //角色基礎體質設定
        this.player.life = 10;
        this.player.no_hurt_time = 0;
        this.player.OnThePlate = undefined;
    },

    Texts:function  () 
    {
        var style = {fill: '#ffaff5', font: '20px Arial', align: 'center'}

        this.text1 = game.add.text(40, 20, '', style);
        this.text2 = game.add.text(280, 20, '', style);
        this.text3 = game.add.text(110, 160, 'Press "Up" to restart\n\nPress "Down" to menu!!', style);
        this.text3.visible = false;
    },

    update: function() 
    {
        if(this.status == 'Game_is_end' && this.cursor.up.isDown) 
        {
            this.restart();
        }
        else if(this.status == 'Game_is_end' && this.cursor.down.isDown)
        {
            game.state.start('menu');
        } 

        if(this.status != 'ingame') return;
            
        game.physics.arcade.collide(this.player, this.platforms, this.effect, null, this);
        game.physics.arcade.collide(this.player, [this.Left_bounder, this.right_bounder]);
        this.Ceiling_Judge(this.player, this.platforms);
        this.checkGameOver();
            
        this.updatePlayer();
        this.updatePlatforms();
        this.updateText();
                
        this.createPlatforms();
    },

    updatePlayer: function () 
    {
        if(this.cursor.left.isDown) 
        {
            this.player.body.velocity.x = -245;
        }
        else if(this.cursor.right.isDown) 
        {
            this.player.body.velocity.x = 245;
        }
        else 
        {
            this.player.body.velocity.x = 0;
        }

        this.PlayerAnimate(this.player);
    },

    //角色動畫設定
    PlayerAnimate:function (player) 
    {
        var x_speed = player.body.velocity.x;
        var y_speed = player.body.velocity.y;
        
        //垂直飛降
        if (x_speed == 0 && y_speed != 0) {
            player.animations.play('fly');
        }
        //定格
        if (x_speed == 0 && y_speed == 0) {
            player.frame = 8;
        }
        //左飛
        if (x_speed < 0 && y_speed > 0) {
            player.animations.play('flyleft');
        }
        //右飛
        if (x_speed > 0 && y_speed > 0) {
            player.animations.play('flyright');
        }
        //左移
        if (x_speed < 0 && y_speed == 0) {
            player.animations.play('left');
        }
        //右移
        if (x_speed > 0 && y_speed == 0) {
            player.animations.play('right');
        }
    },

    updatePlatforms:function() 
    {
        for(var i=0; i< this.platforms.length; i++) 
        {
            var platform = this.platforms[i];
            platform.body.position.y -= 2.5;

            //向錢幣消失
            if(platform.body.position.y <= -20)
            {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },
    
    createPlatforms: function() 
    {
        if(game.time.now > this.lastTime + 600) 
        {
            this.lastTime = game.time.now;
            this.Unit_Platform();
            this.distance += 1;
        }
        if(game.time.now > this.count_time + 2400) 
        {
            this.count_time = game.time.now;
            this.count += 1 ;
            game.global.score -= 1;//用負數計分
        }
    },

    Unit_Platform: function() 
    {
        var platform;
        var x = Math.random()*(400-136) + 20;
        var y = 400;
        var rand = Math.random() * 100;
    
        //這段參考open-source賦圖
        if(rand < 18) 
        {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }
        else if (rand < 41) 
        {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);//針對尖刺的特別設定
        } 
        else if (rand < 51) 
        {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } 
        else if (rand < 60) 
        {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        }
        else if (rand < 80) 
        {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        }
        else 
        {
            platform = game.add.sprite(x, y, 'normal');
        }
    
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        this.platforms.push(platform);
    
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.right = false;
    },
        
    effect:function (player, platform) 
    {
        //向右
        if(platform.key == 'conveyorRight') 
        {
            player.body.x += 2;
            if (player.OnThePlate !== platform) 
            {
                if(player.life < 10) 
                {
                    player.life += 1;
                }
                this.normalSound.play();
                player.OnThePlate = platform;
            }
        }
        //向左
        if(platform.key == 'conveyorLeft') {
            player.body.x -= 2;
            if (player.OnThePlate !== platform) {
                if(player.life < 10) {
                    player.life += 1;
                }
                this.normalSound.play();
                player.OnThePlate = platform;
            }
        }
        //尖刺
        if(platform.key == 'nails') {
            if (player.OnThePlate !== platform) {
                player.life -= 3;
                player.OnThePlate = platform;
                game.camera.flash(0xff0000, 100);
                this.hurtSound.play();
            }
        }
        //普通
        if(platform.key == 'normal') {
            if (player.OnThePlate !== platform) {
                if(player.life < 10) {
                    if(player.life == 7 || player.life == 1)
                    {
                        game.add.tween(player.scale).to({x:0.5,y:0.5},2000).yoyo(true).start();
                    }
                    player.life += 1;          
                }
               
                this.normalSound.play();
                player.OnThePlate = platform;
            }
        }

        //彈跳臺
        if(platform.key == 'trampoline') {
            platform.animations.play('jump');
            if (player.OnThePlate !== platform) {
                if(player.life < 10) {
                    
                    if(player.life == 8 || player.life == 2 )
                    {
                        game.camera.flash(0xffffff, 2000);
                        this.earSound.play();
                    }
                    player.life += 1;
                }
                
                player.OnThePlate = platform;
            }
            this.jumpSound.play();
            player.body.velocity.y = -255;
        }
        //翻轉臺
        if(platform.key == 'fake') {
            if(player.OnThePlate !== platform) {
                if(player.life < 10) {
                    if(player.life == 4)
                    {
                        game.add.tween(player.scale).to({x:2.1,y:2.1},2000).yoyo(true).start();
                    } 
                    player.life += 1;
                }
                this.verseSound.play();
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.OnThePlate = platform;
            }
        }
        
    },   
                  
    Ceiling_Judge:function (player,platform) 
    {
        if(player.body.y < 0) 
        {
            this.hurtSound.play();

            if(player.body.velocity.y < 0) 
            {
                player.body.velocity.y = 0;                
            }
            
            if(game.time.now > player.no_hurt_time) 
            {
                player.life -= 3;
                game.camera.flash(0xff0000, 150);
                player.no_hurt_time = game.time.now + 500;
            }
         
            player.body.checkCollision.down = false;
        }
        else
        {
                player.body.checkCollision.down = true;
        }
    },


    updateText:function  () 
    {
        this.text2.setText('Base: ' + this.count + 'F');
        this.text1.setText('Life: ' + this.player.life);
    },        
        
    checkGameOver:function  () 
    {
        if(this.player.life <= 0 || !this.player.inWorld) 
        {
            this.Game_is_end();
        }
    },
        
    Game_is_end:function  () 
    {
        this.text3.visible = true;
        if(this.die == false)
        {
            game.camera.shake(0.02, 500);
            this.die = true;
        } 

        this.platforms.forEach(function(temp) {temp.destroy()});
        this.platforms = [];

        this.deadSound.play();
        this.backSound.stop();

        setTimeout(function() 
        {
            var person = prompt('Please enter your name', 'Name');
            if(person!=null){
                console.log('check');
                var profiles = {
                  name: person,
                  score: game.global.score
                };  
                var Update = {};
                var newKey = firebase.database().ref().child('feedback').push().key;
                Update['/feedback/'+newKey]=profiles;
                firebase.database().ref().update(Update).then(function(){
                    console.log('check_twice');
                });
                
            }
        }, 1500);
        
        this.status = 'Game_is_end';
    },
        
    restart :function  () 
    {
        this.text3.visible = false;

        this.distance = 0;
        this.count = 0;
        this.die = false;

        this.backSound.play();

        this.Player();
        this.status = 'ingame';

        game.global.score = 0;
    }
};



