var loadState = 
{
    preload: function () 
    {
        game.add.image(0, 0, 'load-bg');
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
        'Now Loading......', { font: '30px Georgia', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);

        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 250, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        
        // Load all game assets

        game.load.image('background', 'assets/background.jpg');
        
        game.load.spritesheet('player', 'assets/player.png', 32, 32); // 主角
        game.load.image('wall', 'assets/wall.png'); // 牆壁
        game.load.image('ceiling', 'assets/ceiling.png'); // 天花板的刺 
        game.load.image('normal', 'assets/stairs.png'); // 藍色平台
        game.load.image('nails', 'assets/stab.png'); // 帶刺平台
        game.load.image('background_in', 'assets/back.png');

        game.load.spritesheet('conveyorRight', 'assets/right.png', 96, 16); // 向右捲動的平台
        game.load.spritesheet('conveyorLeft', 'assets/left.png', 96, 16); // 向左捲動的平台
        game.load.spritesheet('trampoline', 'assets/jump.png', 96, 22); // 彈簧墊
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36); // 翻轉的大平台

        game.load.audio('dead', 'assets/死亡.wav');
        game.load.audio('jump_', 'assets/彈簧.wav');
        game.load.audio('futsu', 'assets/普通.wav');
        game.load.audio('hurt', 'assets/受傷2.wav');
        game.load.audio('verse', 'assets/翻轉.wav');
        game.load.audio('music', 'assets/背景.wav');
        game.load.audio('ear', 'assets/耳鳴.wav');

        game.load.image('menu-bg', 'assets/menu-bg.jpg');
        game.load.image('buttons', 'assets/button.png',265,75);

    },
    create: function() 
    {
        setTimeout(function () {
            game.state.start("menu");
          }, 1000);
    
    }
};