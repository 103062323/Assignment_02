var bootState = 
{
    preload: function()
    {
        // Load the progress bar image.
        game.load.image('progressBar', 'assets/bar2.png');
        game.load.image('load-bg', 'assets/load-bg2.jpg');

    },

    create: function()
    {
         // Set some game settings.
         game.stage.backgroundColor = '#3498db';
         game.physics.startSystem(Phaser.Physics.ARCADE);
         game.renderer.renderSession.roundPixels = true;
         // Start the load state.
         game.state.start('load');
    }


}