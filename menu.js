var menuState = {

    create: function() 
    {
        // Add a background image
        game.add.image(0, 0, 'menu-bg');
        
        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, -40, 'Down to Done',
        { font: '50px Georgia', fill: '#ffe9fc' });
        nameLabel.anchor.setTo(0.5, 0.5);
        
        // Explain how to start the game
        var startLabel = game.add.text(game.width/2, game.height-40,
        '"Enter" or Click "Play" to start', { font: '25px Arial', fill: '#ffe9fc' });
        startLabel.anchor.setTo(0.5, 0.5);
        
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        upKey.onDown.add(this.start, this);

        // Create a tween on the label
        var tween = game.add.tween(nameLabel);
        // Change the y position of the label to 80 in 1 sec.
        tween.to({y: 80}, 1000).easing(Phaser.Easing.Exponential.Out).start();
        // Start the tween
        tween.start();
        var btnPlay = game.add.button(260,155,'buttons',this.clickMe,this,0,1,2,3);
        btnPlay.anchor.set(0.5,0.5);

        game.add.tween(startLabel).to({angle: -3}, 500).to({angle:3}, 1000).to({angle: 0}, 500).delay(500).start();

        var score_count = firebase.database().ref('/feedback/').orderByChild('score').limitToFirst(3);
        score_count.once('value',function(snapshot){
            var style2 = {fill: '#fff9b7', font: 'italic bold 25px Georgia', align: 'center'};
            var style = {fill: ' #6bfff1', font: '22px Georgia', align: 'center'};
            var text_all = [];
            snapshot.forEach(function(childSnapshot)
            {
                //console.log( childSnapshot.val().name);
                text_all.push([childSnapshot.val().name,childSnapshot.val().score*-1]);             
                //console.log(childSnapshot.val().name);
            })
            var text4 = game.add.text(260, 210, 'Leader        Score' , style2);
                
            text4.anchor.setTo(0.5,0.5);
            var text5 = game.add.text(195, 295, text_all[0][0] + '\n'
                                              + text_all[1][0] + '\n' 
                                              + text_all[2][0] + '\n', style);
            text5.anchor.setTo(0.5,0.5);
            var text6 = game.add.text(328, 295, text_all[0][1] + '\n'
                                              + text_all[1][1] + '\n' 
                                              + text_all[2][1] + '\n', style);

            text6.anchor.setTo(0.5,0.5);
             //console.log(child_data.name +' '+ child_data.score +'\n');
        })

        
        
    },
    clickMe:function()
    {
        console.log("success!");
        game.state.start('main');
    },
    start: function() 
    {
        // Start the actual game
        game.state.start('main');
    },
};   

