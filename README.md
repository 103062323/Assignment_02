# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

# Report

## to-do List

|Element|Score|Done(Y/N)|
|:-:|:-:|:-:|
|Complete game process|20%|Y|
|Follow the basic rules|15%|Y|
|Correct physical properties|15%|Y|
|Interesting traps or special mechanisms|10%|Y|
|Additional sound effects and UI|10%|Y|
|Real-time database & Leaderboard|10%|Y|
|Appearance (subjective)|10%|Y|
|Other creative features|10%|Y|  
  
## Demonstration  

* **Complete game process (Loading page=> Start menu => Game view => Game over => quit or play again)**
    * 參照了Lab的State轉換切出了Loading畫面、簡易的Start選單(Play按鈕)  
      遊戲本體流程、Gameover後可以輸入名字(database)，以及決定回到menu或是再玩一次。
    * Start選單中有前三高分的玩家名稱以及其分數(成功達成的樓層)顯示。

* **Follow the basic rules.**
    * 基本上符合大致的規則，但對於血量和回血判定有一些修改。
    * 血量從原版的12->10，扣血量從-5->-3，主要是為了後續features設定的方便。
    * 若血量未滿10，採到任何平臺都能回覆+1血量，直至回滿為止，但彈跳臺同一個只能回一滴。
    * 板子出現的頻率比原版降低了一些，主要是為了讓在尖刺不那麼多的情況下依然維持難度。

* **Correct physical properties**
    * 人物有正確的掉落重力，採到板子有正確的碰撞偵測，踩到特殊板子時也會有相對應的物理改變。
    * 若碰觸到天花板時，有寫一個function使他短暫失去碰撞偵測進而掉落，否則他會卡死在天花板上。  
    * 人物左飛掉落、右飛掉落、直墜、停止、左右行走等都有加入正確的animation。

* **Interesting traps or special mechanisms**
    
    * 依照原版的玩法增加了左右滾動平臺和尖刺平臺。
    * 由於open source中有滾動平臺的sprite sheet，所以也為其做了向左向右持續滾向的loop動畫。
    * 踩到尖刺除了扣血之外，亦用了camera effect做出受傷時的閃光特效。

* **Additional sound effects and UI / Appearance**

    * 本體遊戲中會有背景音樂，遊戲開始後數秒開始播放、死亡後隨即停止音樂。
    * 普通音效有：踩踏聲(普通、滾輪地板)、受傷聲(上下尖刺)、死亡聲(墜落或生命<=0)
    * 特別音效則有：翻轉聲(翻轉地板)、彈跳聲(彈簧平臺)、耳鳴聲(特殊規則)。
    * 值得一提的是，以上音效因為找不到合法的開放資源，所以全部都是下載原版遊戲後自行內錄修剪的QQ
    * 特效有：受傷時的閃光特效、觸發特殊規則的閃光特效、短暫縮小、死亡時的震動特效、標題文字的特效。
    * 在UI設計上根據各個state和遊戲風格做了統一的規劃，整體背景圖調性偏暗、文字偏亮色。

* **Real-time database & Leaderboard**

    * 玩家於死亡後可以輸入自己的名字，並且記錄進firebase realtime database中的'feedback'裡。
       
       <img src="assets/1.png" width="300px" height="180px"></img>
    * 如上，玩家輸入的名字會放進'name'中，而分數則是設成global的形式計算且放入score中。
    * 為了取值方便，database的分數計算和本體(遊玩時所見)的分數計算速度相同、但是數值剛好差一個負號。 
  
      <img src="assets/2.png" ></img>
    * 如此一來當用limitToFirst(3)去比對分數時，就能將最負的child(最高分的人)取出，然後再將其乘上-1放入陣列即可。  

      <img src="assets/3.png" ></img>
    * 最後將database中所取出前三高分的人名和分數用text.add的方式放入menu中，進而完成leaderboard。

* **Other creative features**
    * 在設計完兩個較容易實作的特殊板子(滾輪+尖刺)且完成基本要求之後，就開始著手研究原版的另外兩個特殊板子－翻轉和彈簧平臺  
        * **翻轉平臺**有一個很大的問題，那就是在播放翻轉動畫的同時必須讓主角向下穿越一陣子，最初的設計使用了和天花板  
    尖刺相似的處理方法，用checkCollision.down=false的方式關閉主角下側的碰撞偵測。但結果卻導致玩家很容易穿越或卡進下一個板子  
    造成很多詭異的bug，於是最後加上了setTimeout配合平臺產生頻率、並且只關掉當前翻轉平臺的上方偵測以達成正確的行為模式。   

       * **彈簧平臺**的設計則要是去對平臺判斷做出更適當的處置，因為它是全遊戲中唯一能讓角色「往上」回溯的設計，此時就要確保不能讓  
         角色回到上方的平臺(違反設計規則)、而且面對天花板的處置必須要和原版規則相似(取消停止偵測，讓它有可能卡死在天花板中)，  
         於是為了這個機制，我大幅更動了跳躍高度、平台頻率、還加了一個變數去判斷角色是否處於相同的彈跳平臺上，進而達成目標。

            <img src="assets/4.png"></img>    


    * 由於當前設計的版本在下方階層越深時並不會增加難度，於是我便讓特定血量予以變化，試圖限制玩家所能選擇的下一個平臺。

        * **縮小限制**：當玩家的血量處於1或者是7滴時，如果踏上了普通的踏板，角色本體將會縮小兩倍2秒，期間將更容易墜亡。  

             <img src="assets/5.png" width="200px" height="196px"></img>

       * **閃光干擾**：當玩家的血量處於2或者是8滴時，倘若踏上了彈簧踏版，整體畫面將會被閃光覆蓋兩秒，且發出閃光彈音效。

            <img src="assets/6.png" width="211px" height="206px"></img>
        
       * **翻轉巨人**：當玩家的血量處於4滴時，倘若踏上了翻轉踏版，角色本體將會放大兩倍2秒，期間將會更難下樓，進而被卡死。

            <img src="assets/7.png" ></img>